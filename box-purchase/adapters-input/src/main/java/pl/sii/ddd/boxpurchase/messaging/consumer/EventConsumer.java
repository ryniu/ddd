package pl.sii.ddd.boxpurchase.messaging.consumer;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import pl.sii.ddd.boxpurchase.appservice.BoxNumber;
import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.appservice.Money;
import pl.sii.ddd.boxpurchase.messaging.consumer.model.BoxCreatedEvent;
import pl.sii.ddd.boxpurchase.boxpurchase.AppServicesApplication;
import pl.sii.ddd.boxpurchase.boxpurchase.dto.Box;


@Component
@Slf4j
@RequiredArgsConstructor
public class EventConsumer {

    private AppServicesApplication appServicesApplication;


    @Autowired
    public EventConsumer(AppServicesApplication appServicesApplication) {
        this.appServicesApplication = appServicesApplication;
    }

    @KafkaListener(
            topics = "${topic.name.box-purchused-event}",
            containerFactory = "eventKafkaListenerContainerFactory"
    )
    public void receiveBoxPurchused(BoxCreatedEvent boxCreatedEvent) {
        log.info("Event received: {}", boxCreatedEvent);

        Box box = new Box(boxCreatedEvent.getName(),
                boxCreatedEvent.getDescription(),
                new Money(boxCreatedEvent.getPrice()),
                boxCreatedEvent.getPackages(),
                new BoxNumber(boxCreatedEvent.getBoxNumber()),
                new CustomerId(boxCreatedEvent.getCustomerId()));
        appServicesApplication.createBox(box);
    }
}
