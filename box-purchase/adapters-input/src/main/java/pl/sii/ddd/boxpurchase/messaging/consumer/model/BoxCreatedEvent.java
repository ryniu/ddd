package pl.sii.ddd.boxpurchase.messaging.consumer.model;

import lombok.*;
import java.math.BigDecimal;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class BoxCreatedEvent {
    private String name;
    private String description;
    private BigDecimal price;
    private int packages;
    private String boxNumber;
    private String customerId;
}
