package pl.sii.ddd.boxpurchase.rest;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;
import pl.sii.ddd.boxpurchase.appservice.BoxNumber;
import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.appservice.Money;
import pl.sii.ddd.boxpurchase.boxpurchase.AppServicesApplication;
import pl.sii.ddd.boxpurchase.boxpurchase.dto.Box;

import java.math.BigDecimal;

@RestController
@RequestMapping("/api/adapter")
public class AdapterResource {

    private AppServicesApplication appServicesApplication;

    @Autowired
    public AdapterResource(AppServicesApplication appServicesApplication) {
        this.appServicesApplication = appServicesApplication;
    }

    @PostMapping("/addbox")
    public void addBox() {
        Box box = new Box("BOX NAME",
                "DESCRIPTIOB",
                new Money(100),
                100,
                new BoxNumber("B_1111"),
                new CustomerId("C_1111"));
        appServicesApplication.createBox(box);
    }
}