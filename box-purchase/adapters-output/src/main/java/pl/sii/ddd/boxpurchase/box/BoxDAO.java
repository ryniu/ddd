package pl.sii.ddd.boxpurchase.box;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BoxDAO extends JpaRepository<BoxEntity, Long> {

    BoxEntity findBoxResultEntitiesByBoxNumber(String boxNumer);
}
