package pl.sii.ddd.boxpurchase.box;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.sii.ddd.boxpurchase.appservice.BoxNumber;
import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.appservice.Money;
import pl.sii.ddd.boxpurchase.boxpurchase.repositories.BoxResultRepository;

@Component
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class BoxJPARepository implements BoxResultRepository {

    @Autowired
    private BoxDAO dao;

    @Override
    public void save(BoxAggregate boxAggregate) {
        BoxEntity boxResultEntity = new BoxEntity();
        boxResultEntity.setBoxNumber(boxAggregate.getBoxNumber().toString());
        boxResultEntity.setDescription(boxAggregate.getName());
        boxResultEntity.setName(boxResultEntity.getName());
        boxResultEntity.setPackages(boxResultEntity.getPackages());
        boxResultEntity.setCustomerId(boxResultEntity.getCustomerId());
        boxResultEntity.setPrice(boxAggregate.getPrice().getAmount());
        this.dao.save(boxResultEntity);
    }

    @Override
    public BoxAggregate retrieve(BoxNumber boxNumber) {
        BoxEntity boxResultEntity = this.dao.findBoxResultEntitiesByBoxNumber(boxNumber.toString());
        return new BoxAggregate.BoxAggregateBuilder(new CustomerId(boxResultEntity.getCustomerId()))
                .name(boxResultEntity.getName())
                .boxNumber(boxResultEntity.getBoxNumber())
                .description(boxResultEntity.getDescription())
                .price(new Money(boxResultEntity.getPrice()))
                .packages(boxResultEntity.getPackages())
                .build();
    }
}
