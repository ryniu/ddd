package pl.sii.ddd.boxpurchase.customer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerDAO extends JpaRepository<CustomerEntity, Long> {

    CustomerEntity findCustomerEntitiesByCustomerId(String customerId);
}
