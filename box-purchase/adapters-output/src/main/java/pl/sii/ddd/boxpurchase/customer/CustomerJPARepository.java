package pl.sii.ddd.boxpurchase.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.boxpurchase.repositories.CustomerResultRepository;

@Component
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class CustomerJPARepository implements CustomerResultRepository {

    @Autowired
    private CustomerDAO dao;

    @Override
    public void save(CustomerAggregate customerAggregate) {
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setCustomerId(customerAggregate.getCustomerId().getCustomerId());
        customerEntity.setCity(customerAggregate.getAddress().getCity());
        customerEntity.setName(customerAggregate.getName());
        customerEntity.setStreet(customerAggregate.getAddress().getStreet());
        customerEntity.setPostCode(customerAggregate.getAddress().getPostCode());

        this.dao.save(customerEntity);
    }

    @Override
    public CustomerAggregate retrieve(CustomerId customerId) {
        CustomerEntity customer = dao.findCustomerEntitiesByCustomerId(customerId.getCustomerId());
        return new CustomerAggregate.CustomerAggregateBuilder()
                .city(customer.getCity())
                .name(customer.getName())
                .street(customer.getStreet())
                .postCode(customer.getPostCode())
                .customerBalance(customer.getBalance())
                .build();
    }
}
