package pl.sii.ddd.boxpurchase.customerBoxInvoice;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sii.ddd.boxpurchase.box.BoxEntity;

public interface CustomerBoxInvoiceDAO extends JpaRepository<CustomerBoxInvoiceEntity, Long> {

    CustomerBoxInvoiceEntity findBoxResultEntitiesByBoxNumberAndCustomerId(String boxNumer, String customerId);
}
