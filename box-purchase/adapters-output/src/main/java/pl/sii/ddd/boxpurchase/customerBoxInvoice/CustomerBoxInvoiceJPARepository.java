package pl.sii.ddd.boxpurchase.customerBoxInvoice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.sii.ddd.boxpurchase.appservice.BoxNumber;
import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.boxpurchase.repositories.CustomerBoxInvoiceRepository;
import pl.sii.ddd.boxpurchase.customer.CustomerDAO;

@Component
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class CustomerBoxInvoiceJPARepository implements CustomerBoxInvoiceRepository {

    @Autowired
    private CustomerBoxInvoiceDAO dao;
    @Override
    public void save(CustomerBoxInvoiceAggregate customerBoxInvoiceAggregate) {
        CustomerBoxInvoiceEntity customerBoxInvoiceEntity = new CustomerBoxInvoiceEntity();
        customerBoxInvoiceEntity.setCustomerId(customerBoxInvoiceAggregate.getCustomerId().getCustomerId());
        customerBoxInvoiceEntity.setBoxNumber(customerBoxInvoiceEntity.getBoxNumber());
        customerBoxInvoiceEntity.setBalance(customerBoxInvoiceAggregate.getInvoiceBalance().toBigDecimal());
        this.dao.save(customerBoxInvoiceEntity);


    }

    @Override
    public CustomerBoxInvoiceAggregate retrieve(CustomerId customerId, BoxNumber boxNumber) {
         CustomerBoxInvoiceEntity customerBoxInvoiceEntity = this.dao.findBoxResultEntitiesByBoxNumberAndCustomerId(boxNumber.toString(), customerId.getCustomerId());

         return new CustomerBoxInvoiceAggregate.CustomerBoxInvoiceAggregateBuilder()
                 .boxNumber(customerBoxInvoiceEntity.getBoxNumber())
                 .invoiceId("")
                 .customerId(customerBoxInvoiceEntity.getCustomerId())
                 .invoiceBalance(customerBoxInvoiceEntity.getBalance())
                 .build();

    }
}
