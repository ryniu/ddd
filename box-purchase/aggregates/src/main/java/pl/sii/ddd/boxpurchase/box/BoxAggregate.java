package pl.sii.ddd.boxpurchase.box;

import pl.sii.ddd.boxpurchase.appservice.BoxNumber;
import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.appservice.Money;
import pl.sii.ddd.boxpurchase.box.Packages;

public class BoxAggregate {
    private final String name;
    private final String description;
    private final Money price;
    private final Packages packages;
    private final BoxNumber boxNumber;
    private final CustomerId customerId;

    public BoxAggregate(BoxAggregateBuilder boxAggregateBuilder) {
        BoxNumber boxNumber = new BoxNumber(boxAggregateBuilder.boxNumber);
        this.name = boxAggregateBuilder.name;
        this.description = boxAggregateBuilder.description;
        this.price = boxAggregateBuilder.price;
        this.packages = boxAggregateBuilder.packages;
        this.customerId = boxAggregateBuilder.customerId;
        this.boxNumber = boxNumber;
    }


    public int howManyPackagesLeft(){
        return packages.getCount();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Money getPrice() {
        return price;
    }

    public Packages getPackages() {
        return packages;
    }

    public BoxNumber getBoxNumber() {
        return boxNumber;
    }

    public CustomerId getCustomerId() {
        return customerId;
    }

    public static class BoxAggregateBuilder {
        private String name;
        private String boxNumber;
        private String description;
        private CustomerId customerId;
        private Money price;
        private Packages packages;

        public BoxAggregateBuilder(CustomerId customerId) {
            this.customerId = customerId;
        }

        public BoxAggregateBuilder packages(int packages) {
            this.packages = new Packages(packages);
            return this;
        }

        public BoxAggregateBuilder price(Money price) {
            this.price = price;
            return this;
        }

        public BoxAggregateBuilder description(String description) {
            this.description = description;
            return this;
        }
        public BoxAggregateBuilder boxNumber(String boxNumber) {
            this.boxNumber = boxNumber;
            return this;
        }

        public BoxAggregateBuilder name(String name) {
            this.name = name;
            return this;
        }

        public BoxAggregate build() {
            return new BoxAggregate(this);
        }
    }
}