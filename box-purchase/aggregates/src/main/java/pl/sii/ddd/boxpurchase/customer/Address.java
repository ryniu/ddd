package pl.sii.ddd.boxpurchase.customer;

class Address {
	private String city;
	private String postCode;
	private String street;

	Address(String street, String postCode, String city) {
		this.city = city;
		this.postCode = postCode;
		this.street = street;
	}

	String getCity() {
		return city;
	}

	String getPostCode() {
		return postCode;
	}

	String getStreet() {
		return street;
	}
}
