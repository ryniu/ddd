package pl.sii.ddd.boxpurchase.customer;

import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.appservice.Money;

import java.math.BigDecimal;

public class CustomerAggregate {
    private final CustomerId customerId;
    private final String name;
    private final Address address;
    private final CustomerBalance balance;
    private CustomerAggregate(CustomerAggregateBuilder builder) {
        Address address = new Address(builder.street, builder.postCode, builder.city);
        this.address = address;
        this.balance = builder.customerBalance;
        this.name = builder.name;
        this.customerId = builder.customerId;
    }

    public CustomerBalance getBalance() {
        return balance;
    }

    CustomerId getCustomerId() {
        return customerId;
    }

    String getName() {
        return name;
    }
    Address getAddress() { return address; }
    public static class CustomerAggregateBuilder {
        private String name;
        private String street;
        private String postCode;
        private String city;
        private CustomerBalance customerBalance;
        private CustomerId customerId;
        public CustomerAggregateBuilder() {

        }
        public CustomerAggregateBuilder name(String name) {
            this.name = name;
            return this;
        }
        public CustomerAggregateBuilder street(String street) {
            this.street = street;
            return this;
        }
        public CustomerAggregateBuilder postCode(String postCode) {
            this.postCode = postCode;
            return this;
        }
        public CustomerAggregateBuilder city(String city) {
            this.city = city;
            return this;
        }
        public CustomerAggregateBuilder customerId(String customerId) {
            this.customerId = new CustomerId(customerId);
            return this;
        }
        public CustomerAggregateBuilder customerBalance(int balance) {
            this.customerBalance = new CustomerBalance(new Money(balance));
            return this;
        }
        public CustomerAggregateBuilder customerBalance(BigDecimal balance) {
            this.customerBalance = new CustomerBalance(new Money(balance));
            return this;
        }

        public CustomerAggregate build() {
            if(this.customerBalance == null) {
                this.customerBalance = new CustomerBalance(new Money());
            }
            return new CustomerAggregate(this);
        }
    }
}
