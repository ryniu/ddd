package pl.sii.ddd.boxpurchase.customer;


import pl.sii.ddd.boxpurchase.appservice.Money;

import java.math.BigDecimal;

class CustomerBalance {
	private Money balance;

	CustomerBalance(BigDecimal balance) {
		this(new Money(balance));
	}
	CustomerBalance(Money balance) {
		this.balance = balance;
	}

	BigDecimal toBigDecimal() {
		return balance.getAmount();
	}
}
