package pl.sii.ddd.boxpurchase.customerBoxInvoice;

import pl.sii.ddd.boxpurchase.appservice.BoxNumber;
import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.appservice.InvoiceId;
import pl.sii.ddd.boxpurchase.appservice.Money;

import java.math.BigDecimal;

public class CustomerBoxInvoiceAggregate {

    private final InvoiceId invoiceId;
    private final CustomerId customerId;
    private final BoxNumber boxNumber;
    private final InvoiceBalance invoiceBalance;

    private CustomerBoxInvoiceAggregate(CustomerBoxInvoiceAggregateBuilder builder) {
        this.invoiceBalance = builder.invoiceBalance;
        this.customerId = builder.customerId;
        this.boxNumber = builder.boxNumber;
        this.invoiceId = builder.invoiceId;
    }

    CustomerId getCustomerId() {
        return customerId;
    }

    BoxNumber getBoxNumber() {
        return boxNumber;
    }

    InvoiceBalance getInvoiceBalance() {
        return invoiceBalance;
    }

    InvoiceId getInvoiceId() {
        return invoiceId;
    }

    public static class CustomerBoxInvoiceAggregateBuilder {
        private BoxNumber boxNumber;
        private InvoiceBalance invoiceBalance;
        private CustomerId customerId;
        private InvoiceId invoiceId;

        public CustomerBoxInvoiceAggregateBuilder() {

        }
        public CustomerBoxInvoiceAggregateBuilder boxNumber(String boxNumber) {
            this.boxNumber = new BoxNumber(boxNumber);
            return this;
        }
        public CustomerBoxInvoiceAggregateBuilder invoiceBalance(BigDecimal balance) {
            this.invoiceBalance = new InvoiceBalance(new Money(balance));
            return this;
        }
        public CustomerBoxInvoiceAggregateBuilder customerId(String customerId) {
            this.customerId = new CustomerId(customerId);
            return this;
        }

        public CustomerBoxInvoiceAggregateBuilder invoiceId(String invoiceId) {
            this.invoiceId = new InvoiceId(invoiceId);
            return this;
        }

        public CustomerBoxInvoiceAggregate build() {
            if(this.invoiceBalance == null) {
                this.invoiceBalance = new InvoiceBalance(new Money());
            }
            return new CustomerBoxInvoiceAggregate(this);
        }
    }

}
