package pl.sii.ddd.boxpurchase.customerBoxInvoice;


import pl.sii.ddd.boxpurchase.appservice.Money;

import java.math.BigDecimal;

class InvoiceBalance {
	private Money balance;

	InvoiceBalance(BigDecimal balance) {
		this(new Money(balance));
	}
	InvoiceBalance(Money balance) {
		this.balance = balance;
	}

	BigDecimal toBigDecimal() {
		return balance.getAmount();
	}
}
