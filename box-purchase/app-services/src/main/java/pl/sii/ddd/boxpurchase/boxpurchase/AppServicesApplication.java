package pl.sii.ddd.boxpurchase.boxpurchase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sii.ddd.boxpurchase.box.BoxAggregate;
import pl.sii.ddd.boxpurchase.customer.CustomerAggregate;
import pl.sii.ddd.boxpurchase.boxpurchase.dto.Box;
import pl.sii.ddd.boxpurchase.boxpurchase.dto.Customer;
import pl.sii.ddd.boxpurchase.boxpurchase.repositories.CustomerBoxInvoiceRepository;
import pl.sii.ddd.boxpurchase.boxpurchase.repositories.BoxResultRepository;
import pl.sii.ddd.boxpurchase.boxpurchase.repositories.CustomerResultRepository;

@Service
public class AppServicesApplication {

    private CustomerBoxInvoiceRepository accountingResultRepository;

    private BoxResultRepository boxResultRepository;

    private CustomerResultRepository customerResultRepository;

    @Autowired
    public AppServicesApplication(CustomerBoxInvoiceRepository accountingResultRepository, BoxResultRepository boxResultRepository, CustomerResultRepository customerResultRepository) {
        this.accountingResultRepository = accountingResultRepository;
        this.boxResultRepository = boxResultRepository;
        this.customerResultRepository = customerResultRepository;
    }

    public void createBox(Box box){
        BoxAggregate boxAggregate = new BoxAggregate.BoxAggregateBuilder(box.getCustomerId())
                .boxNumber(box.getBoxNumber().toString())
                .packages(box.getPackages())
                .name(box.getName())
                .price(box.getPrice())
                .description(box.getDescription())
                .build();


        boxResultRepository.save(boxAggregate);
    }

    public void createCustomer(Customer customer){
        CustomerAggregate customerAggregate = new CustomerAggregate.CustomerAggregateBuilder()
                .city(customer.getCity())
                .name(customer.getName())
                .postCode(customer.getPostCode())
                .street(customer.getStreet())
                .build();


        customerResultRepository.save(customerAggregate);
    }

    public void boxPurchased(Box box) {

    }

    public void accountCustomerBalance(Box box, Customer customer){

    }

}
