package pl.sii.ddd.boxpurchase.boxpurchase.dto;

import pl.sii.ddd.boxpurchase.appservice.BoxNumber;
import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.appservice.Money;

public class Box {

    private String name;
    private String description;
    private Money price;
    private int packages;
    private BoxNumber boxNumber;
    private CustomerId customerId;

    public Box(String name, String description, Money price, int packages, BoxNumber boxNumber, CustomerId customerId) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.packages = packages;
        this.boxNumber = boxNumber;
        this.customerId = customerId;
    }

    public Box(String name, String description, Money price, int packages, BoxNumber boxNumber) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.packages = packages;
        this.boxNumber = boxNumber;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Money getPrice() {
        return price;
    }

    public int getPackages() {
        return packages;
    }

    public BoxNumber getBoxNumber() {
        return boxNumber;
    }

    public CustomerId getCustomerId() {
        return customerId;
    }
}
