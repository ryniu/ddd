package pl.sii.ddd.boxpurchase.boxpurchase.dto;

import pl.sii.ddd.boxpurchase.appservice.Money;

public class CustomerBoxInvoice {

    private Money amount;

    public CustomerBoxInvoice(Money amount) {
        this.amount = amount;
    }

    public Money getAmount() {
        return amount;
    }
}
