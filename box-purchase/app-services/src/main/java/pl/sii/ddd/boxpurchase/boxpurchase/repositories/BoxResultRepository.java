package pl.sii.ddd.boxpurchase.boxpurchase.repositories;

import pl.sii.ddd.boxpurchase.appservice.BoxNumber;
import pl.sii.ddd.boxpurchase.box.BoxAggregate;
public interface BoxResultRepository {
    void save(BoxAggregate boxAggregate);
    BoxAggregate retrieve(BoxNumber boxNumber);
}