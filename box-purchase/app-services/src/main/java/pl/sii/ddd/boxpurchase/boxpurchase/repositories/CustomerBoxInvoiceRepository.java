package pl.sii.ddd.boxpurchase.boxpurchase.repositories;

import pl.sii.ddd.boxpurchase.appservice.BoxNumber;
import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.customerBoxInvoice.CustomerBoxInvoiceAggregate;

public interface CustomerBoxInvoiceRepository {
    void save (CustomerBoxInvoiceAggregate customerBoxInvoiceAggregate);
    CustomerBoxInvoiceAggregate retrieve(CustomerId customerId, BoxNumber boxNumber);
}