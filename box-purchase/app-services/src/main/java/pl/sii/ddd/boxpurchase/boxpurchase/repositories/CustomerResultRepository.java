package pl.sii.ddd.boxpurchase.boxpurchase.repositories;

import pl.sii.ddd.boxpurchase.appservice.CustomerId;
import pl.sii.ddd.boxpurchase.customer.CustomerAggregate;

public interface CustomerResultRepository {
    void save(CustomerAggregate customerAggregate);

    CustomerAggregate retrieve(CustomerId customerId);
}
