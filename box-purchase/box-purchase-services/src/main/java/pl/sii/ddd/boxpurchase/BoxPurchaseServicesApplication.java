package pl.sii.ddd.boxpurchase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;

@SpringBootApplication(exclude = KafkaAutoConfiguration.class)
public class BoxPurchaseServicesApplication {
    public static void main(String[] args) {
        SpringApplication.run(BoxPurchaseServicesApplication.class, args);
    }
}
