package pl.sii.ddd.boxpurchase.appservice;

public class BoxNumber {
	private String boxNumber;

	public BoxNumber(String boxNumber) {
		this.boxNumber = boxNumber;
	}

	@Override
	public String toString() {
		return boxNumber;
	}
}
