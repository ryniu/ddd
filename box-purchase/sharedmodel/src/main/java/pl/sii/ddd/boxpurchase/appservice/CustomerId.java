package pl.sii.ddd.boxpurchase.appservice;

import lombok.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;


public class CustomerId {
	private String customerId;

	public CustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerId() {
		return customerId;
	}
}
