package pl.sii.ddd.boxpurchase.appservice;


public class InvoiceId {
	private String invoiceId;

	public InvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}
}
