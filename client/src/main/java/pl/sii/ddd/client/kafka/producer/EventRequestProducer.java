package pl.sii.ddd.client.kafka.producer;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import pl.sii.ddd.client.kafka.producer.model.BoxCreatedEvent;

@Component
public class EventRequestProducer {

    @Autowired
    private KafkaTemplate<String, BoxCreatedEvent> eventKafkaTemplate;

    @Value("${topic.name.box-purchused-event}")
    private String topic;

    public void sendMessage(BoxCreatedEvent msg) {
        eventKafkaTemplate.send(topic, msg);
    }
}
