package pl.sii.ddd.client.kafka.producer.model;

import java.math.BigDecimal;
import lombok.*;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class BoxCreatedEvent {
    private String name;
    private String description;
    private BigDecimal price;
    private int packages;
    private String boxNumber;
    private String customerId;
}
