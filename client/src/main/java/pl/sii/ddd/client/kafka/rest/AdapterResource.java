package pl.sii.ddd.client.kafka.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.sii.ddd.client.kafka.producer.EventRequestProducer;
import pl.sii.ddd.client.kafka.producer.model.BoxCreatedEvent;

import java.math.BigDecimal;

@RestController
@RequestMapping("/api/adapter")
public class AdapterResource {
    @Autowired
    private EventRequestProducer eventRequestProducer;

    @PostMapping("/addbox")
    public void addBox() {
        BoxCreatedEvent boxCreatedEvent = new BoxCreatedEvent();
        boxCreatedEvent.setBoxNumber("bn_1");
        boxCreatedEvent.setName("BN1");
        boxCreatedEvent.setDescription("DESC BN1");
        boxCreatedEvent.setPackages(10);
        boxCreatedEvent.setPrice(new BigDecimal(100));
        boxCreatedEvent.setCustomerId("1000");


        eventRequestProducer.sendMessage(boxCreatedEvent);
    }
}